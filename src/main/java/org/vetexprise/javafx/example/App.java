package org.vetexprise.javafx.example;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

/**
 * JavaFX App Example
 */
public class App extends Application {

    /**
     * Сцена приложения
     *
     */
    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        
        // Загрузка файла FXML
        Parent parent = loadFXML();
        
        // Формирование сцены
        scene = new Scene(parent, 2048, 768);
        stage.setScene(scene);
        stage.show();
    }

 
    
    /**
     * Загрузка файла FXML
     *
     *
     * @return
     * @throws IOException
     */
    private Parent loadFXML() throws IOException {
        URL location = getClass().getResource("table.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        Parent root = (Parent) fxmlLoader.load(location.openStream());
        return root;
    }

    
    /**
     * Точка входа в приложение
     * @param args 
     */
    public static void main(String[] args) {
        launch();
    }
    
    
    

}