/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vetexprise.javafx.example.table;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author vaganovdv
 */
public class PersonController  implements Initializable{

    
    // Основная модель данных
    private  final Set<Person> personSet = new HashSet<>(); 
    
    
    // Отображение таблицы [список персон]
    @FXML
    private  TableView<Person> personTableView = new TableView<>();
    
    private TableColumn<Person, String> person_sureName;  
    private TableColumn<Person, String> person_firstName;  
    private TableColumn<Person, String> person_middleName;      
    
    // Данные таблицы <personTableView>
    private final ObservableList<Person> personData = FXCollections.observableArrayList();
    
    private Person selectedPerson = null;
    
    @FXML 
    TextField firstNameField;
    
    @FXML 
    TextField sureNameField;
    
    @FXML 
    TextField middleNameField;
    
    
    @FXML
    Label statusLabel;
    
    @FXML
    Button saveButton;
    
    @FXML
    Button deleteButton;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {            
       initPersonTable();  
       
    }
    
    
    /**
     * Первоначальная инициализация таблицы персон
     */
    private void initPersonTable ()
    {
     
       System.out.println("Инициализация таблицы персон ...");
        
        // Формирование столбцов
        
        // Формирование столбца Фамилия
        person_sureName = new TableColumn("Фамилия");
        person_sureName.setPrefWidth(180);
        person_sureName.setMinWidth(180);        
        person_sureName.setCellValueFactory(new PropertyValueFactory<Person, String>("sureName"));
        
        // Формирование столбца Имя
        person_firstName = new TableColumn("Имя");
        person_firstName.setPrefWidth(120);
        person_firstName.setMinWidth(120);
        person_firstName.setCellValueFactory(new PropertyValueFactory<Person, String>("firstName"));
        
        
        // Формирование столбца Отчество
        person_middleName = new TableColumn("Отчество");
        person_middleName.setPrefWidth(220);
        person_middleName.setMinWidth(220);
        person_middleName.setCellValueFactory(new PropertyValueFactory<Person, String>("middleName"));
        
        
        // Добавление  столбцов к таблице
        personTableView.getColumns().setAll(
                person_sureName,
                person_firstName,
                person_middleName               
        );
        
        // Размещение  пустого списка персон в таблице
        personTableView.setItems(personData);
        // Вывод сообщения об 
        personTableView.setPlaceholder(new Label("Нет данных о персонах"));
        
        // Обновление содержимого таблицы (с пустым списком)
        personTableView.refresh();
         
        personTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Person>()
            { // Слушатель событий
                @Override
                public void changed(ObservableValue<? extends Person> oldPerson, Person value, Person newPerson)
                { // Обработка события выбора пункта списка
                    if (newPerson != null)
                    {
                        System.out.println("[Таблица персон]: выбор строки "+newPerson.getSureName());  
                        selectedPerson = newPerson;                        
                    }

                }
            });
        
        
        System.out.println("Инициализация таблицы персон ЗАВЕРШЕНА");
    }
    
    @FXML
    private void addPerson() throws IOException {
        updateStatus("Добавление персоны ....");
        Person p = new Person(sureNameField.getText(), firstNameField.getText(), middleNameField.getText());
        addPerson(p);
    }
    
     
    @FXML
    private void deletePerson() throws IOException {
        updateStatus("Удаление персоны ....");
        if (selectedPerson != null)
        {
            updateStatus("Удалить персону ["+selectedPerson.getSureName()+"] ?");            
            deletePerson(selectedPerson);
        }

    }
    
    /**
     * Удаление  персоны
     *
     * @param list
     */
    private void deletePerson(Person person) {
        
        if (person != null) {
            Task task = new Task<Void>() {
                @Override
                public Void call() throws Exception {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                           if (personData.contains(person))
                           {                                                            
                            personData.remove(person);
                            personTableView.setItems(personData);                            
                            personTableView.refresh();
                            updateStatus("Удалена персона:  " + person.getSureName());
                           }
                        }
                    });
                    return null;
                }
            };
            Thread th = new Thread(task);
            th.setDaemon(true);
            th.start();
        }
    }
    
    
    
    /**
     * Добавление песоны
     *
     * @param list
     */
    private void addPerson(Person person) {
        
        if (person != null) {
            Task task = new Task<Void>() {
                @Override
                public Void call() throws Exception {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            personData.add(person);
                            personTableView.setItems(personData);
                            personTableView.getSelectionModel().selectLast();
                            personTableView.refresh();
                            updateStatus("Добавлена персона:  " + person.getSureName());
                        }
                    });
                    return null;
                }
            };
            Thread th = new Thread(task);
            th.setDaemon(true);
            th.start();
        }
    }

    private void updateStatus(String message) {
        if (Platform.isFxApplicationThread()) {
            statusLabel.setText(message);
        } else {
            Platform.runLater(()
                    -> {
                statusLabel.setText(message);
            });
        }
    }
      
     
    
}
